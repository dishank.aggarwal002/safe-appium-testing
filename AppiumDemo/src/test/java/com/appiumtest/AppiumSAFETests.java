package com.appiumtest;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.connection.ConnectionState;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.touch.offset.PointOption;
import okhttp3.Connection;
import com.appiumtest.DeviceVariables;
import com.appiumtest.DeviceVariables.device;

import java.util.*;
public class AppiumSAFETests {
	
	public static AndroidDriver<MobileElement> driver;
	public static WebDriverWait wait;
	public static TouchAction touchAction;
	
	
	public static void main(String[] args) throws Exception {
		
		Scanner sc=new Scanner(System.in);  
		System.out.print("Enter Device no for testing \n"
				+ "1. : pixel-4\n"
				+ "2. : pixel-4a\n");  
		int deviceNo= sc.nextInt();  

		
		DeviceVariables deviceVar = new DeviceVariables(device.values()[deviceNo-1]);
		System.out.println(deviceVar.deviceName);
		giveQuiz(deviceVar);
//		openNavigationPane(deviceVar);
//		offlineQuiz(deviceVar);
//		clearAnswers(deviceVar);
//		openCalculator(deviceVar);
//		exitAppDuringQuiz(deviceVar);
	}
	
	
	private static void exitAppDuringQuiz(DeviceVariables device) {
		// Exit button: 524, 1265
		// Back: 637, 1265
		
		initQuiz(device);
		
		// Select first question from list
//		String qNo = "Q. 1";
//		String firstQuestionText = "//android.widget.TextView[@text='" + qNo + "']";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(firstQuestionText)));
//		WebElement firstQuestionXpathBtn = driver.findElementByXPath(firstQuestionText);
//		firstQuestionXpathBtn.click();
		String marksid = "com.iitb.cse.arkenstone.safe_beta:id/marks";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		// Wait for 3 seconds
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
		
		// Exit from quiz
		driver.pressKey(new KeyEvent().withKey(AndroidKey.HOME));
		
		// Wait for 3 seconds
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
		
		// Go back to quiz
		TouchAction touchAction = new TouchAction(driver);
//		touchAction.tap(PointOption.point(544, 950)).perform();
		touchAction.tap(PointOption.point(device.backtoquizX, device.backtoquizY)).perform();
		
		
		
		// Wait for 3 seconds
		try {
			Thread.sleep(3000);
		} catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
		}
		
		submitQuiz();
	}
	
	
	private static void openCalculator(DeviceVariables device) {
		initQuiz(device);
		
		// Select first question from list
//		String qNo = "Q. 1";
//		String firstQuestionText = "//android.widget.TextView[@text='" + qNo + "']";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(firstQuestionText)));
//		WebElement firstQuestionXpathBtn = driver.findElementByXPath(firstQuestionText);
//		firstQuestionXpathBtn.click();
		
		
		String marksid = "com.iitb.cse.arkenstone.safe_beta:id/marks";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		try {
			Thread.sleep(500);
			TouchAction touchAction = new TouchAction(driver);
			touchAction.tap(PointOption.point(972, 1589)).perform();
			Thread.sleep(500);
			touchAction.tap(PointOption.point(972, 1154)).perform();
			Thread.sleep(500);
			System.out.println("Calculator opened");
			
		} catch (Exception e) {
			System.out.println("Execption: " + e.getMessage());
		}
		
		addNumbers(123, 14);
		
		submitQuiz();
	}
	
	
	private static void addNumbers(int numOne, int numTwo) {
		enterNumberInCalc(numOne);
		
		String addBtnId = "com.iitb.cse.arkenstone.safe_beta:id/bt_add";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(addBtnId)));
		WebElement addBtn = driver.findElementById(addBtnId);
		addBtn.click();
		
		enterNumberInCalc(numTwo);
		
		String equalsBtnId = "com.iitb.cse.arkenstone.safe_beta:id/bt_equals";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(equalsBtnId)));
		WebElement equalsBtn = driver.findElementById(equalsBtnId);
		equalsBtn.click();
			
		driver.pressKey(new KeyEvent().withKey(AndroidKey.BACK));
	}
	
	
	private static void enterNumberInCalc(int d) {
		d = reverseNum(d);
		
		while (d > 0) {
			int digit = d % 10;
			String digitId = "com.iitb.cse.arkenstone.safe_beta:id/bt_digit" + digit;
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id(digitId)));
			WebElement digitBtn = driver.findElementById(digitId);
			digitBtn.click();
			try {
				Thread.sleep(300);
			} catch (Exception e) {
				System.out.println("Exception: " + e.getMessage());
			}
			d /= 10;
		}
	}
	
	private static int reverseNum(int d) {
		int rev = 0;
		while (d > 0) {
			rev *= 10;
			rev += (d % 10);
			d /= 10;
		}
		return rev;
	}
	
	
	private static void clearAnswers(DeviceVariables device) throws Exception {
		initQuiz(device);
		
		// Select first question from list
//		String qNo = "Q. 1";
//		String firstQuestionText = "//android.widget.TextView[@text='" + qNo + "']";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(firstQuestionText)));
//		WebElement firstQuestionXpathBtn = driver.findElementByXPath(firstQuestionText);
//		firstQuestionXpathBtn.click();
		
		String marksid = "com.iitb.cse.arkenstone.safe_beta:id/marks";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		
		// Select answer for each quiz
		
		
		// MCQ
		String mcqOptionXPath = "//android.widget.LinearLayout[4]/android.widget.CheckBox";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(mcqOptionXPath)));
		WebElement mcqOptionElement = driver.findElementByXPath(mcqOptionXPath);
		mcqOptionElement.click();
		
		gotoNextQuestion(device);
		
		// SCQ
				String scqOptionXPath = "//android.widget.RadioButton[1]";
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(scqOptionXPath)));
				WebElement scqOptionElement = driver.findElementByXPath(scqOptionXPath);
				scqOptionElement.click();
				
				gotoNextQuestion(device);
		
		// FIB
		String fibXPath = "//android.widget.EditText";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(fibXPath)));
		WebElement fibElement = driver.findElementByXPath(fibXPath);
		fibElement.sendKeys("abc");
		
		gotoNextQuestion(device);
		
		// NUM
		String numericalXPath = "//android.widget.EditText";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(numericalXPath)));
		WebElement numericalElement = driver.findElementByXPath(numericalXPath);
		numericalElement.sendKeys("10");
		
		gotoNextQuestion(device);
		
//		// IMG
//		String imageXPath = "//android.widget.ImageView[@content-desc=\"Thumbnail of Original image\"]";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(imageXPath)));
//		WebElement imageBtnElement = driver.findElementByXPath(imageXPath);
//		imageBtnElement.click();
//		try {
//			Thread.sleep(500);
//			touchAction.tap(PointOption.point(541, 2088)).perform();
//			gotoNextQuestion();
//			System.out.println("Image clicked");
//			
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
		
		gotoPrevQuestion(device);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		clearAnswerForFIBNum();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		gotoPrevQuestion(device);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		clearAnswerForFIBNum();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		gotoPrevQuestion(device);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		clearAnswerForScqMcq();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		gotoPrevQuestion(device);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		clearAnswerForScqMcq();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		submitQuiz();
	}
	
	private static void gotoPrevQuestion(DeviceVariables device) {
		try {
			Thread.sleep(500);
			TouchAction touchAction = new TouchAction(driver);
//			touchAction.tap(PointOption.point(225, 1792)).perform();
			touchAction.tap(PointOption.point(device.prevX, device.prevY)).perform();
			Thread.sleep(500);
			System.out.println("prev question");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static void clearAnswerForScqMcq() {
		try {
			Thread.sleep(500);
			TouchAction touchAction = new TouchAction(driver);
			touchAction.tap(PointOption.point(972, 1589)).perform();
			Thread.sleep(500);
			touchAction.tap(PointOption.point(972, 1440)).perform();
			Thread.sleep(500);
			System.out.println("Answer cleared");
		} catch (Exception e) {
			System.out.println("Execption: " + e.getMessage());
		}
	}
	
	
	private static void clearAnswerForFIBNum() {
		try {
			Thread.sleep(500);
			TouchAction touchAction = new TouchAction(driver);
			touchAction.tap(PointOption.point(972, 1589)).perform();
			Thread.sleep(500);
			touchAction.tap(PointOption.point(972, 1440)).perform();
			Thread.sleep(500);
			String confirmBtnXPath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]";
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(confirmBtnXPath)));
			WebElement confirmBtn = driver.findElementByXPath(confirmBtnXPath);
			confirmBtn.click();
//			Thread.sleep(500);
//			touchAction.tap(PointOption.point(960, 1939)).perform();
			Thread.sleep(500);
			System.out.println("Answer cleared");
		} catch (Exception e) {
			
		}
	}
	
	// /hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/androidx.appcompat.widget.LinearLayoutCompat/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]
	
	
	private static void offlineQuiz(DeviceVariables device) {
		initQuiz(device);
		
		// Select first question from list
//		String qNo = "Q. 1";
//		String firstQuestionText = "//android.widget.TextView[@text='" + qNo + "']";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(firstQuestionText)));
//		WebElement firstQuestionXpathBtn = driver.findElementByXPath(firstQuestionText);
//		firstQuestionXpathBtn.click();
		
		String marksid = "com.iitb.cse.arkenstone.safe_beta:id/marks";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		
		// Turn off wifi
		driver.toggleWifi();
		driver.toggleData();
		
		// Attempt quiz
		
		
		// MCQ
		String mcqOptionXPath = "//android.widget.LinearLayout[4]/android.widget.CheckBox";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(mcqOptionXPath)));
		WebElement mcqOptionElement = driver.findElementByXPath(mcqOptionXPath);
		mcqOptionElement.click();
		
		gotoNextQuestion(device);
		
		
		// SCQ
				String scqOptionXPath = "//android.widget.RadioButton[1]";
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(scqOptionXPath)));
				WebElement scqOptionElement = driver.findElementByXPath(scqOptionXPath);
				scqOptionElement.click();
				
				gotoNextQuestion(device);
		
		String submitBtnXpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[4]";
		WebElement submitBtn = driver.findElementByXPath(submitBtnXpath);
		submitBtn.click();
		
		String confirmSubmitYesId = "android:id/button1";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(confirmSubmitYesId)));
		WebElement confirmSubmitYesBtn = driver.findElementById(confirmSubmitYesId);
		confirmSubmitYesBtn.click();
		
		try {
			Thread.sleep(45000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		String exitQuizBtnId = "com.iitb.cse.arkenstone.safe_beta:id/exit_quiz";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(exitQuizBtnId)));
		WebElement exitQuizBtn = driver.findElementById(exitQuizBtnId);
		exitQuizBtn.click();
		
		// Turn on wifi
		driver.toggleWifi();
		driver.toggleData();
		
		try {
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		// Try to resubmit quiz
		// com.iitb.cse.arkenstone.safe_beta:id/btn_more_options
		// com.iitb.cse.arkenstone.safe_beta:id/ib_refresh_button
		String resetBtnId = "com.iitb.cse.arkenstone.safe_beta:id/ib_refresh_button";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(resetBtnId)));
		WebElement resetBtn = driver.findElementById(resetBtnId);
		resetBtn.click();
		
	}
	
	public static void giveQuiz(DeviceVariables deviceVar) {
		System.out.println(deviceVar.deviceName);
		initQuiz(deviceVar);
		
		// Select first question from list
//		String qNo = "Q. 1";
//		String firstQuestionText = "//android.widget.TextView[@text='" + qNo + "']";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(firstQuestionText)));
//		WebElement firstQuestionXpathBtn = driver.findElementByXPath(firstQuestionText);
//		firstQuestionXpathBtn.click();
		
		
		// MCQ
				String mcqOptionXPath = "//android.widget.LinearLayout[4]/android.widget.CheckBox";
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(mcqOptionXPath)));
				WebElement mcqOptionElement = driver.findElementByXPath(mcqOptionXPath);
				mcqOptionElement.click();
//				System.out.print(driver.getPageSource());
				gotoNextQuestion(deviceVar);
				
//				TouchAction touchAction=new TouchAction(driver);
//				touchAction.tap(PointOption.point(1000, 1200)).perform();
//				TouchAction touchAction2=new TouchAction(driver);
//				
//				touchAction2.tap(PointOption.point(913, 1828)).perform();

//				(new TouchAction(driver)).tap(PointOption.point(1043, 1792)).perform();

				
//				
//				String nextPath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout[4]/android.widget.ImageView";
////				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(nextPath)));
//				WebElement nextOptionElement = driver.findElementByXPath(nextPath);
//				WebElement nextOptionElement = driver.findElement(By.xpath("//android.widget.ImageView[@bounds='[886,1794][949,1857]']"));
//
//				nextOptionElement.click();
		
		// SCQ
		String scqOptionXPath = "//android.widget.RadioButton[1]";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(scqOptionXPath)));
		WebElement scqOptionElement = driver.findElementByXPath(scqOptionXPath);
		scqOptionElement.click();
		
		gotoNextQuestion(deviceVar);
		
		
//		(new TouchAction(driver)).tap(PointOption.point(1043, 1792)).perform();
		
		// FIB
		String fibXPath = "//android.widget.EditText";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(fibXPath)));
		WebElement fibElement = driver.findElementByXPath(fibXPath);
		fibElement.sendKeys("abc");
		
		gotoNextQuestion(deviceVar);
//		(new TouchAction(driver)).tap(PointOption.point(1043, 1792)).perform();
		
		// NUM
		String numericalXPath = "//android.widget.EditText";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(numericalXPath)));
		WebElement numericalElement = driver.findElementByXPath(numericalXPath);
		numericalElement.sendKeys("10");
		
		gotoNextQuestion(deviceVar);
//		(new TouchAction(driver)).tap(PointOption.point(1043, 1792)).perform();
		
//		// IMG
//		String imageXPath = "//android.widget.ImageView[@content-desc=\"Thumbnail of Original image\"]";
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(imageXPath)));
//		WebElement imageBtnElement = driver.findElementByXPath(imageXPath);
//		imageBtnElement.click();
//		try {
//			Thread.sleep(500);
//			touchAction.tap(PointOption.point(541, 2088)).perform();
//			gotoNextQuestion();
//			System.out.println("Image clicked");
//			
//		} catch (Exception e) {
//			System.out.println(e.getMessage());
//		}
	
		submitQuiz();

	}
	
	private static void submitQuiz() {
		String submitBtnXpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.LinearLayout/android.widget.LinearLayout/android.widget.TextView[4]";
		WebElement submitBtn = driver.findElementByXPath(submitBtnXpath);
		submitBtn.click();
		
		String confirmSubmitYesId = "android:id/button1";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(confirmSubmitYesId)));
		WebElement confirmSubmitYesBtn = driver.findElementById(confirmSubmitYesId);
		confirmSubmitYesBtn.click();
		
		String exitQuizBtnId = "com.iitb.cse.arkenstone.safe_beta:id/exit_quiz";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(exitQuizBtnId)));
		WebElement exitQuizBtn = driver.findElementById(exitQuizBtnId);
		exitQuizBtn.click();
	}
	
	
	private static void initQuiz(DeviceVariables device) {
		// Setting up desired capabilities
		DesiredCapabilities caps = new DesiredCapabilities();
		caps.setCapability("platformName", "android");
		caps.setCapability("platformVersion", device.androidV);
		caps.setCapability("deviceName", device.deviceName);
		caps.setCapability("automationName", "UiAutomator2");
		caps.setCapability("appPackage", "com.iitb.cse.arkenstone.safe_beta");
		caps.setCapability("appActivity", "com.iitb.cse.arkenstone.safe_beta.course.CourseListActivity");
		caps.setCapability("autoGrantPermissions", "true");
		caps.setCapability("autoAcceptAlerts", "true");
		caps.setCapability("enableMultiWindows", true);

//		caps.setCapability("udid", "emulator-5554");
		
		
		
		
		// Setting up the URL
		try {
			URL url = new URL("http://0.0.0.0:4723/wd/hub/");
			driver = new AndroidDriver<>(url, caps);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		touchAction = new TouchAction(driver);
		
		
		String currentActivity = driver.currentActivity();
		// Try login, if not logged in
		if (currentActivity.equals(".LoginPinActivity")) {
					
			WebElement emailIdEditText = driver.findElementByXPath("//android.widget.EditText[@content-desc=\"Email id\"]");
			emailIdEditText.sendKeys("dishank.aggarwal002@gmail.com");
			
			WebElement passwordEditText = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.widget.LinearLayout[3]/android.widget.FrameLayout/android.widget.EditText");
			passwordEditText.sendKeys("Hello@123gu");
			
			WebElement loginBtn = driver.findElementById("com.iitb.cse.arkenstone.safe_beta:id/login_btn");
			loginBtn.click();
		}
		
		wait = new WebDriverWait(driver, 10);
		
		// Select TST196 course
		String courseName = "CS669";
		String courseCardViewXPath = "//android.widget.FrameLayout[@content-desc=\"" + courseName + "course\"]/android.view.ViewGroup";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(courseCardViewXPath)));
		WebElement courseItem = driver.findElementByXPath(courseCardViewXPath);
		courseItem.click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")));
		WebElement okAlertbox = driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button");
		okAlertbox.click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button")));
//		driver.findElementByXPath("/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button").click();
//		
		
		
		// Select quiz
		String quizId = "CS669.TL";
		String quizItemXPath = "//android.widget.FrameLayout[@content-desc=\"" + quizId + "quiz\"]/android.view.ViewGroup/android.widget.Button";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(quizItemXPath)));
		WebElement quizItemStartBtn = driver.findElementByXPath(quizItemXPath);
		quizItemStartBtn.click();
		
		// Tap grant-all and give VPN permission if present
//		try {	
//			String grantAllBtnXPath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.view.ViewGroup/android.widget.Button[3]";
//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(grantAllBtnXPath)));
//			WebElement grantAllBtn = driver.findElementByXPath(grantAllBtnXPath);
//			grantAllBtn.click();
//			
//			String vpnPermBtnXPath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]";
//			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vpnPermBtnXPath)));
//			WebElement vpnPermBtn = driver.findElementByXPath(vpnPermBtnXPath);
//			vpnPermBtn.click();			
//		} catch (Exception e) {
//			System.out.println("Exception," + e.getMessage());
//		}
		
		
		// Enter password
		String quizPassword = "CBGSHJEC";
		String quizPasswordEtId = "com.iitb.cse.arkenstone.safe_beta:id/et_quiz_key_input";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(quizPasswordEtId)));
		WebElement quizPasswordEt = driver.findElementById(quizPasswordEtId);
		quizPasswordEt.sendKeys(quizPassword);
		
		// Start quiz
		String startQuizBtnId = "com.iitb.cse.arkenstone.safe_beta:id/btn_start_quiz";
		WebElement startQuizBtn = driver.findElementById(startQuizBtnId);
		startQuizBtn.click();
	}
	
	
	private static void openNavigationPane(DeviceVariables device) throws Exception {
		initQuiz(device);
		
		
		String marksid = "com.iitb.cse.arkenstone.safe_beta:id/marks";
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id(marksid)));
		System.out.print(driver.getPageSource());
		Thread.sleep(500);
//		(new TouchAction(driver)).tap(PointOption.point(539, 1792)).perform();
		(new TouchAction(driver)).tap(PointOption.point(device.navX, device.navY)).perform();
		Thread.sleep(500);
		System.out.println("Nav bar");
		int qNo = 1;
		String qNoStr = "", navQuestionText = "";
		System.out.print(qNo);
		while (true) {
			
			qNoStr = "Q. " + qNo;
			navQuestionText = "//android.widget.TextView[@text='" + qNoStr + "']";
			
			try {
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(navQuestionText)));
			} catch (Exception e) {
				break;
			}
			
			WebElement navQuestionXPath = driver.findElementByXPath(navQuestionText);
			navQuestionXPath.click();
			
			try {
				Thread.sleep(500);
				touchAction.tap(PointOption.point(539, 1792)).perform();
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			qNo++;
		}
		
		// Navigate to last question and exit quiz
		qNoStr = "Q. " + (--qNo);
		navQuestionText = "//android.widget.TextView[@text='" + qNoStr + "']";
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(navQuestionText)));
		} catch (Exception e) {
			
		}
		
		WebElement navQuestionXPath = driver.findElementByXPath(navQuestionText);
		navQuestionXPath.click();
		
		submitQuiz();
	}
	
	private static void gotoNextQuestion(DeviceVariables device) {
		try {
			Thread.sleep(500);
//			(new TouchAction(driver)).tap(PointOption.point(1043, 1792)).perform();
			(new TouchAction(driver)).tap(PointOption.point(device.nextX, device.nextY)).perform();
			Thread.sleep(500);
			System.out.println("Next question");
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}

