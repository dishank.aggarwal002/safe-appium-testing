package com.appiumtest;

public class DeviceVariables {
	
	//deviceName denotes the device name you want to perform the test on.
	String deviceName ;	
	
	//String denoting the android version of the device. 
	String androidV;
	
	/*
	 * Integer containing the x coordinate of the screen of the device for tap point of the NEXT button of the quiz.
	 * */
	int nextX;
	
	/*
	 * Integer containing the y coordinate of the screen of the device for tap point of the NEXT button of the quiz.
	 * */
	int nextY;
	
	/*
	 * Integer containing the x coordinate of the screen of the device for tap point of the PREV  button of the quiz.
	 * */
	int prevX;
	
	/*
	 * Integer containing the y coordinate of the screen of the device for tap point of the PREV  button of the quiz.
	 * */
	int prevY;
	
	/*
	 * Integer containing the x coordinate of the screen of the device for tap point of the NAV button of the quiz.
	 * */
	int navX;
	
	/*
	 * Integer containing the y coordinate of the screen of the device for tap point of the NAV button of the quiz.
	 * */
	int navY;
	
	
	/*
	 * Integer containing the x coordinate of the screen of the device for tap point of the "Go back to quiz" button of the red alert window.
	 * */
	int backtoquizX;
	
	/*
	 * Integer containing the y coordinate of the screen of the device for tap point of the "Go back to quiz" button of the red alert window.
	 * */
	int backtoquizY;
	
	
	
	/*
	 * Enum variable denoting the device you want to perform the testing.
	 * */
	public enum device {
		PIXEL_4,
		PIXEL_4A
	}
	
	
	
	
	
	
	public DeviceVariables(device dev) {
		
		switch(dev){
		
		case PIXEL_4:
			deviceName = "pixel-4";
			androidV = "10";
			nextX = 1043;
			nextY = 1792;
			prevX = 225;
			prevY = 1792;
			navX = 539;
			navY =1792;
			backtoquizX = 544;
			backtoquizY= 950;
			break;
			
		case PIXEL_4A:
			deviceName = "pixel-4a";
			androidV = "12";
			nextX = 1043;
			nextY = 2147;
			prevX = 225;
			prevY = 2147;
			navX = 539;
			navY =2147;
			backtoquizX = 544;
			backtoquizY= 950;
			
			
		}
		
//		if (code == 1) {
//			deviceName = "pixel-4";
//			androidV = "10";
//			nextX = 1043;
//			nextY = 1792;
//			prevX = 225;
//			prevY = 1792;
//			navX = 539;
//			navY =1792;
//			backtoquizX = 544;
//			backtoquizY= 950;
//			
//			
//			
//		}
//		else {
//			deviceName = "pixel-4a";
//			androidV = "12";
//			nextX = 1043;
//			nextY = 2147;
//			prevX = 225;
//			prevY = 2147;
//			navX = 539;
//			navY =2147;
//			backtoquizX = 544;
//			backtoquizY= 950;
//		}
//		
	}
}
